import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlacklistFormularioComponent } from './blacklist-formulario/blacklist-formulario.component';

const routes: Routes = [ {path:'', component:BlacklistFormularioComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
